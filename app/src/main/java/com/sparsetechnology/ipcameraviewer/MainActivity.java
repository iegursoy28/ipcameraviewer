package com.sparsetechnology.ipcameraviewer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements CamListFragment.OnCamListFragmentListener, PlayerFragment.OnRestartRequestListener {
    @BindView(R.id.main_player_container)
    public FrameLayout playerContainer;

    @BindView(R.id.main_list_container)
    public FrameLayout listContainer;

    @BindView(R.id.tool_nav_icon)
    public ImageView toolIcon;

    @BindView(R.id.tool_title)
    public TextView toolTitle;

    @OnClick(R.id.tool_nav_icon)
    public void onClickToggle(View v) {
        if (listContainer != null) {
            if (listContainer.getVisibility() == View.VISIBLE)
                Utils.UIHelper.slideToLeft(listContainer);
            else
                Utils.UIHelper.slideToRight(listContainer);
        }
    }

    private CamModel currentCam;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.main_list_container, CamListFragment.newInstance(1)).commit();

        listContainer.bringToFront();
        toolIcon.bringToFront();

        Utils.UIHelper.OnSwipeTouchListener onSwipeTouchListener = new Utils.UIHelper.OnSwipeTouchListener(this, new Utils.UIHelper.OnSwipeTouchListener.SwipeListener() {
            @Override
            public void onSwipeRight() {
                if (listContainer != null && listContainer.getVisibility() != View.VISIBLE)
                    Utils.UIHelper.slideToRight(listContainer);
            }

            @Override
            public void onSwipeLeft() {
                if (listContainer != null && listContainer.getVisibility() == View.VISIBLE)
                    Utils.UIHelper.slideToLeft(listContainer);
            }

            @Override
            public void onSwipeTop() {

            }

            @Override
            public void onSwipeBottom() {

            }
        });

        listContainer.setOnTouchListener(onSwipeTouchListener);
        playerContainer.setOnTouchListener(onSwipeTouchListener);

        if (savedInstanceState != null) {
            String cam_json = savedInstanceState.getString("saved_cam", null);
            if (cam_json != null) {
                onClickInteraction(CamModel.fromJson(cam_json));
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (currentCam != null)
            outState.putString("saved_cam", currentCam.toJson());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClickInteraction(CamModel item) {
        currentCam = item;
        toolTitle.setText(item.getEndpoint());
        getSupportFragmentManager().beginTransaction().replace(R.id.main_player_container, PlayerFragment.newInstance(item.toJson())).commit();
    }

    @Override
    public void onRestartRequest() {
        if (currentCam != null)
            getSupportFragmentManager().beginTransaction().replace(R.id.main_player_container, PlayerFragment.newInstance(currentCam.toJson())).commit();
    }
}
