package com.sparsetechnology.ipcameraviewer;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;

import java.util.Arrays;

public class Utils {
    public static class UIHelper {
        public static void toggleViewLeftRight(View view) {
            if (view.getVisibility() != View.VISIBLE)
                slideToRight(view);
            else
                slideToLeft(view);
        }

        public static void toggleViewUpDown(View view) {
            if (view.getVisibility() != View.VISIBLE)
                slideToUp(view);
            else
                slideToDown(view);
        }

        // To animate view slide out from left to right
        public static void slideToRight(View view) {
            TranslateAnimation animate = new TranslateAnimation(-view.getWidth(), 0, 0, 0);
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.VISIBLE);
        }

        // To animate view slide out from right to left
        public static void slideToLeft(View view) {
            TranslateAnimation animate = new TranslateAnimation(0, -view.getWidth(), 0, 0);
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.GONE);
        }

        // To animate view slide out from left to right
        public static void slideToUp(View view) {
            TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.VISIBLE);
        }

        // To animate view slide out from right to left
        public static void slideToDown(View view) {
            TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.GONE);
        }

        public static class OnSwipeTouchListener implements View.OnTouchListener {

            private final GestureDetector gestureDetector;

            public OnSwipeTouchListener(Context ctx, SwipeListener swipeListener) {
                gestureDetector = new GestureDetector(ctx, new GestureListener(swipeListener));
            }

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }

            private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
                private static final int SWIPE_THRESHOLD = 100;
                private static final int SWIPE_VELOCITY_THRESHOLD = 100;

                private final SwipeListener swipeListener;

                GestureListener(SwipeListener swipeListener) {
                    this.swipeListener = swipeListener;
                }

                @Override
                public boolean onDown(MotionEvent e) {
                    return true;
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    boolean result = false;
                    try {
                        float diffY = e2.getY() - e1.getY();
                        float diffX = e2.getX() - e1.getX();
                        if (Math.abs(diffX) > Math.abs(diffY)) {
                            if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                                if (diffX > 0) {
                                    if (swipeListener != null) swipeListener.onSwipeRight();
                                } else {
                                    if (swipeListener != null) swipeListener.onSwipeLeft();
                                }
                                result = true;
                            }
                        } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffY > 0) {
                                if (swipeListener != null) swipeListener.onSwipeBottom();
                            } else {
                                if (swipeListener != null) swipeListener.onSwipeTop();
                            }
                            result = true;
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    return result;
                }
            }

            public interface SwipeListener {
                void onSwipeRight();

                void onSwipeLeft();

                void onSwipeTop();

                void onSwipeBottom();
            }
        }
    }

    public static String[] getNames(Class<? extends Enum<?>> e) {
        return Arrays.toString(e.getEnumConstants()).replaceAll("^.|.$", "").replaceAll("UNRECOGNIZED", "").split(", ");
    }
}
