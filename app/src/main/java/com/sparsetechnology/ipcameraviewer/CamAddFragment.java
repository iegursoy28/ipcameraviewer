package com.sparsetechnology.ipcameraviewer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CamAddFragment extends DialogFragment {
    private OnCamAddListener mListener;

    @BindView(R.id.fca_radio_group)
    RadioGroup radio_group;

    @BindView(R.id.fca_layout_ip)
    TextInputLayout layout_ip;
    @BindView(R.id.fca_txt_host)
    TextInputEditText txt_host;

    @BindView(R.id.fca_layout_port)
    TextInputLayout layout_port;
    @BindView(R.id.fca_txt_port)
    TextInputEditText txt_port;

    private boolean isGrpc = false;

    @OnClick(R.id.fca_btn_cancel)
    public void onClickCancel() {
        this.dismiss();
    }

    @OnClick(R.id.fca_btn_ok)
    public void onClickOk() {
        String host = txt_host.getText().toString();
        String port = txt_port.getText().toString();
        host = host.trim();
        port = port.trim();

        if (host.isEmpty() || (isGrpc && port.isEmpty())) {
            Toast.makeText(getContext(), "Please fill all inputs", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isGrpc)
            onButtonPressed(CamModel.newInstanceGRPC(host, Integer.parseInt(port)));
        else
            onButtonPressed(CamModel.newInstanceRTPS(host));

        this.dismiss();
    }

    public CamAddFragment() {
    }

    public static CamAddFragment newInstance() {
        return new CamAddFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cam_add, container, false);
        ButterKnife.bind(this, view);
        radio_group.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.fca_radio_grpc:
                    isGrpc = true;
                    break;
                case R.id.fca_radio_rtsp:
                default:
                    isGrpc = false;
                    break;
            }

            if (isGrpc) {
                layout_ip.setHint(getString(R.string.host));
                layout_port.setVisibility(View.VISIBLE);
            } else {
                layout_ip.setHint(getString(R.string.url));
                layout_port.setVisibility(View.GONE);
            }
        });
        return view;
    }

    public void setListener(OnCamAddListener mListener) {
        this.mListener = mListener;
    }

    public void onButtonPressed(CamModel cam) {
        if (mListener != null)
            mListener.onAddCam(cam);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnCamAddListener {
        void onAddCam(CamModel cam);
    }
}
