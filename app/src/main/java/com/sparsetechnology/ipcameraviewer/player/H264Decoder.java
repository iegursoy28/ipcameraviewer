package com.sparsetechnology.ipcameraviewer.player;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.util.Log;
import android.view.Surface;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

public class H264Decoder extends Thread {
    private static final String TAG = H264Decoder.class.getSimpleName();

    private static final String MIME_TYPE = "video/avc";
    public static final double FRAME_RATE = 12.5;
    public static final double FRAME_RATE_TS = 1000.0 / FRAME_RATE;
    private static final int TIMEOUT_USEC = 10000;
    private static final int WAIT_TIME = 300;

    private int MediaCodecWidth = 1920;
    private int MediaCodecHeight = 1080;

    private MediaCodec decoder;
    private StreamData mData;
    private Surface surface;
    private double frameRate = FRAME_RATE;
    private double frameRateTs = FRAME_RATE_TS;

    private boolean isRun;

    public H264Decoder(StreamData mData, Surface surface) {
        this.mData = mData;
        this.surface = surface;
        this.setName(TAG);
    }

    public void setFrameRate(double frameRate) {
        this.frameRate = frameRate;
        this.frameRateTs = 1000.0 / frameRate;
    }

    private long last_ts;

    @Override
    public void run() {
        isRun = true;
        if (!prepareDecoder())
            return;

        int frameIndex = 0;
        StreamData.RAWFrame frame;

        while (isRun) {
            if (mData.getFrames().size() <= 0)
                continue;

            frame = mData.getFrame(frameIndex);
            mData.removeFrame(frameIndex);

            int inIndex;
            while ((inIndex = decoder.dequeueInputBuffer(TIMEOUT_USEC)) < 0)
                if (!isRun) break;

            if (inIndex >= 0) {
                Objects.requireNonNull(decoder.getInputBuffer(inIndex)).put(frame.frameData);
                decoder.queueInputBuffer(inIndex, 0, frame.frameData.length, 0, MediaCodec.CRYPTO_MODE_UNENCRYPTED);

                MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
                int outIndex = decoder.dequeueOutputBuffer(info, TIMEOUT_USEC);
                switch (outIndex) {
                    case MediaCodec.INFO_TRY_AGAIN_LATER:
                        Log.d("Buffer info", "INFO_TRY_AGAIN_LATER");
                        break;
                    case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                        Log.d("Buffer info", "INFO_OUTPUT_FORMAT_CHANGED: New format: " + decoder.getOutputFormat());
                        break;
                    case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                        Log.d("Buffer info", "INFO_OUTPUT_BUFFERS_CHANGED");
                        break;
                    default:
                        decoder.releaseOutputBuffer(outIndex, true);
                        while (System.currentTimeMillis() - last_ts < frameRateTs - 5) {
                            try {
                                Thread.sleep(5);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        last_ts = System.currentTimeMillis();
                        break;
                }
            }
        }

        decoder.setParameters(null);
        decoder.stop();
        decoder.release();
        decoder = null;
    }

    /**
     * Prepare decoder
     *
     * @return Successful prepare return value true
     */
    private boolean prepareDecoder() {
        try {
            while ((mData.getHeader_sps() == null || mData.getHeader_pps() == null || mData.getFrames().size() <= 0) && isRun) {
                Thread.sleep(WAIT_TIME);
                Log.i(TAG, "Waiting...");
            }

            MediaFormat mediaFormat = MediaFormat.createVideoFormat(MIME_TYPE, MediaCodecWidth, MediaCodecHeight);
            mediaFormat.setByteBuffer("csd-0", ByteBuffer.wrap(mData.getHeader_sps()));
            mediaFormat.setByteBuffer("csd-1", ByteBuffer.wrap(mData.getHeader_pps()));
            //mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);

            decoder = MediaCodec.createDecoderByType(MIME_TYPE);
            decoder.configure(mediaFormat, surface, null, 0);
            decoder.start();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void release() {
        isRun = false;
        this.interrupt();
        Log.d(TAG, "Releasing decoder.");
    }

    private static int findNextStart(byte[] data, int off) {
        if (off >= data.length)
            return -1;

        for (int i = off; i < data.length; i++)
            if ((data[i] == 0 && data[i + 1] == 0 && data[i + 2] == 0 && data[i + 3] == 1)
                    || (data[i] == 0 && data[i + 1] == 0 && data[i + 2] == 1))
                return i;

        return -1;
    }

    public static byte[] findSPS(byte[] data) {
        for (int i = 0; i < data.length - 5; i++) {
            if (data[i] == 0 &&
                    data[i + 1] == 0 &&
                    data[i + 2] == 0 &&
                    data[i + 3] == 1 &&
                    (data[i + 4] & 0x1f) == 0x07) {
                int next = findNextStart(data, i + 5);
                return Arrays.copyOfRange(data, i, next > i ? next : data.length);
            }
        }

        return null;
    }

    public static byte[] findPPS(byte[] data) {
        for (int i = 0; i < data.length - 5; i++) {
            if (data[i] == 0 &&
                    data[i + 1] == 0 &&
                    data[i + 2] == 0 &&
                    data[i + 3] == 1 &&
                    (data[i + 4] & 0x1f) == 0x08) {
                int next = findNextStart(data, i + 5);
                return Arrays.copyOfRange(data, i, next > i ? next : data.length);
            }
        }

        return null;
    }
}
