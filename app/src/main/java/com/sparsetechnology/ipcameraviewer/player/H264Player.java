package com.sparsetechnology.ipcameraviewer.player;

import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import vms.Nvr;

public class H264Player implements SurfaceHolder.Callback {
    private static final String TAG = H264Player.class.getSimpleName();

    private StreamData mData;
    private H264Decoder mDecoder;
    private double mFrameRate = H264Decoder.FRAME_RATE;

    private SurfaceHolder holder;
    private boolean isRun = false;

    public H264Player(SurfaceView surfaceView) {
        holder = surfaceView.getHolder();
        surfaceView.getHolder().addCallback(this);
        mData = new StreamData();
    }

    public void setFrameRate(double mFrameRate) {
        this.mFrameRate = mFrameRate;
    }

    public void useFrameData(Nvr.StreamBuffer buffer) {
        try {
            if (!isRun)
                throw new Exception("Is not running.");

            byte[] readInData = buffer.getData().toByteArray();
            if (readInData.length <= 0)
                throw new Exception("Size error.");

            mData.useFrameData(readInData, buffer.getTs());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "Surface created.");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.holder = holder;
        Log.d(TAG, "Surface changed.");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stop();
    }

    public void start() {
        isRun = true;
        if (mDecoder == null && holder.getSurface() != null) {
            mDecoder = new H264Decoder(mData, holder.getSurface());
            mDecoder.setFrameRate(mFrameRate);
            mDecoder.start();
        }
    }

    public void stop() {
        isRun = false;
        if (mDecoder != null) {
            mDecoder.release();
            mDecoder = null;
        }
    }
}