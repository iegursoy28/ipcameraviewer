package com.sparsetechnology.ipcameraviewer;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sparsetechnology.ipcameraviewer.CamListFragment.OnCamListFragmentListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CamModelRecyclerViewAdapter extends RecyclerView.Adapter<CamModelRecyclerViewAdapter.ViewHolder> {
    private final Context mContext;
    private final OnCamListFragmentListener mListener;

    private ArrayList<CamModel> mCamList;

    private boolean isEditMode = false;

    public CamModelRecyclerViewAdapter(@NonNull Context context, OnCamListFragmentListener listener) {
        mContext = context;
        mListener = listener;
        mCamList = getCamList();
    }

    private final String PREF_NAME = String.format("%s.%s", getClass().getSimpleName(), "PREF_NAME");
    private final String CAM_LIST_PREF_NAME = String.format("%s.%s", getClass().getSimpleName(), "CAM_LIST_PREF_NAME");

    private ArrayList<CamModel> getCamList() {
        SharedPreferences preferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Set<String> strings = preferences.getStringSet(CAM_LIST_PREF_NAME, new HashSet<>());

        ArrayList<CamModel> cams = new ArrayList<>();
        if (strings == null)
            return cams;

        for (String string : strings)
            cams.add(CamModel.fromJson(string));

        return cams;
    }

    private void setCamList() {
        SharedPreferences preferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Set<String> sets = new HashSet<>();
        for (CamModel c : mCamList)
            sets.add(c.toJson());

        editor.putStringSet(CAM_LIST_PREF_NAME, sets);
        editor.apply();
    }

    public void toggleEditMode() {
        isEditMode = !isEditMode;
        notifyDataSetChanged();
    }

    public boolean isEditMode() {
        return isEditMode;
    }

    public void addCamModel(@NonNull CamModel cam) {
        mCamList.add(cam);
        notifyDataSetChanged();
        setCamList();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item_cam, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mCamList.get(position);
        holder.mUrl.setText(mCamList.get(position).getEndpoint());
        holder.mDel.setVisibility(isEditMode ? View.VISIBLE : View.GONE);

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("url", holder.mItem.getEndpoint());
                clipboard.setPrimaryClip(clip);

                Toast.makeText(mContext, mContext.getResources().getString(R.string.copy_to_clipboard), Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        holder.mView.setOnClickListener((v) -> {
            if (null != mListener)
                mListener.onClickInteraction(holder.mItem);
        });

        holder.mDel.setOnClickListener((v) -> {
            if (mCamList.remove(holder.mItem)) {
                notifyDataSetChanged();
                setCamList();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCamList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mUrl;
        public final ImageButton mDel;
        public CamModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mUrl = (TextView) view.findViewById(R.id.item_cam_url);
            mDel = (ImageButton) view.findViewById(R.id.item_cam_del_btn);
        }
    }
}
