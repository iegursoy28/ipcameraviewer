package com.sparsetechnology.ipcameraviewer;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

public class HelperUtils {
    private static String TAG = HelperUtils.class.getSimpleName();

    /**
     * Get human readable date
     *
     * @param ts milliseconds
     * @return date string
     */
    public static String getDateFromEpoc(long ts) {
        return new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss.SSS", Locale.getDefault()).format(new java.util.Date(ts));
    }

    private static int findNextStart(byte[] data, int off) {
        if (off >= data.length)
            return -1;

        for (int i = off; i < data.length; i++)
            if ((data[i] == 0 && data[i + 1] == 0 && data[i + 2] == 0 && data[i + 3] == 1)
                    || (data[i] == 0 && data[i + 1] == 0 && data[i + 2] == 1))
                return i;

        return -1;
    }

    /**
     * Get SPS in frame data
     *
     * @param data frame bytes
     * @return SPS byte array
     */
    public static byte[] findSPS(byte[] data) {
        for (int i = 0; i < data.length - 5; i++) {
            if (data[i] == 0 &&
                    data[i + 1] == 0 &&
                    data[i + 2] == 0 &&
                    data[i + 3] == 1 &&
                    (data[i + 4] & 0x1f) == 0x07) {
                int next = findNextStart(data, i + 5);
                return Arrays.copyOfRange(data, i, next > i ? next : data.length);
            }
        }

        return null;
    }

    /**
     * Get PPS in frame data
     *
     * @param data frame bytes
     * @return PPS byte array
     */
    public static byte[] findPPS(byte[] data) {
        for (int i = 0; i < data.length - 5; i++) {
            if (data[i] == 0 &&
                    data[i + 1] == 0 &&
                    data[i + 2] == 0 &&
                    data[i + 3] == 1 &&
                    (data[i + 4] & 0x1f) == 0x08) {
                int next = findNextStart(data, i + 5);
                return Arrays.copyOfRange(data, i, next > i ? next : data.length);
            }
        }

        return null;
    }


    public static class UIHelper {
        public static void toggleViewLeftRight(View view) {
            if (view.getVisibility() != View.VISIBLE)
                slideToRight(view);
            else
                slideToLeft(view);
        }

        public static void toggleViewUpDown(View view) {
            if (view.getVisibility() != View.VISIBLE)
                slideToUp(view);
            else
                slideToDown(view);
        }

        // To animate view slide out from left to right
        public static void slideToRight(View view) {
            TranslateAnimation animate = new TranslateAnimation(-view.getWidth(), 0, 0, 0);
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.VISIBLE);
        }

        // To animate view slide out from right to left
        public static void slideToLeft(View view) {
            TranslateAnimation animate = new TranslateAnimation(0, -view.getWidth(), 0, 0);
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.GONE);
        }

        // To animate view slide out from left to right
        public static void slideToUp(View view) {
            TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.VISIBLE);
        }

        // To animate view slide out from right to left
        public static void slideToDown(View view) {
            TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
            animate.setDuration(500);
            animate.setFillAfter(false);
            view.startAnimation(animate);
            view.setVisibility(View.GONE);
        }

        public static class OnSwipeTouchListener implements View.OnTouchListener {

            private final GestureDetector gestureDetector;

            public OnSwipeTouchListener(Context ctx, SwipeListener swipeListener) {
                gestureDetector = new GestureDetector(ctx, new GestureListener(swipeListener));
            }

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }

            private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
                private static final int SWIPE_THRESHOLD = 100;
                private static final int SWIPE_VELOCITY_THRESHOLD = 100;

                private final SwipeListener swipeListener;

                GestureListener(SwipeListener swipeListener) {
                    this.swipeListener = swipeListener;
                }

                @Override
                public boolean onDown(MotionEvent e) {
                    return true;
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    boolean result = false;
                    try {
                        float diffY = e2.getY() - e1.getY();
                        float diffX = e2.getX() - e1.getX();
                        if (Math.abs(diffX) > Math.abs(diffY)) {
                            if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                                if (diffX > 0) {
                                    if (swipeListener != null) swipeListener.onSwipeRight();
                                } else {
                                    if (swipeListener != null) swipeListener.onSwipeLeft();
                                }
                                result = true;
                            }
                        } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffY > 0) {
                                if (swipeListener != null) swipeListener.onSwipeBottom();
                            } else {
                                if (swipeListener != null) swipeListener.onSwipeTop();
                            }
                            result = true;
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    return result;
                }
            }

            public interface SwipeListener {
                void onSwipeRight();

                void onSwipeLeft();

                void onSwipeTop();

                void onSwipeBottom();
            }
        }
    }
}
