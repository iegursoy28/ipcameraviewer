package com.sparsetechnology.ipcameraviewer;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.VLCVideoLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import m11.M11Control;
import m11.VideoControlGrpc;
import timber.log.Timber;

public class PlayerFragment extends Fragment {
    private static final String ARG_CAM_JSON = "arg_cam_json";

    private static final boolean USE_TEXTURE_VIEW = false;
    private static final boolean ENABLE_SUBTITLES = true;

    private VLCVideoLayout mVideoLayout = null;
    private SurfaceView mVideoSurface = null;

    private TextView zoom_focus_pos = null;
    private ImageButton video_btn_toggle = null;

    private LibVLC mLibVLC = null;
    private MediaPlayer mMediaPlayer = null;
    private GrpcVideoPlayer player = null;

    private OnRestartRequestListener mListener;

    private Context mContext;
    private CamModel mCam;

    private ManagedChannel channel;

    public PlayerFragment() {
    }

    public static PlayerFragment newInstance(String cam_json) {
        PlayerFragment fragment = new PlayerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CAM_JSON, cam_json);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCam = CamModel.fromJson(getArguments().getString(ARG_CAM_JSON));

            if (mCam != null) {
                try {
                    channel = ManagedChannelBuilder.forAddress(mCam.getHost(), 40001).usePlaintext().build();
                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private final View.OnClickListener onPlayerButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (channel == null)
                return;

            VideoControlGrpc.VideoControlBlockingStub stub = VideoControlGrpc.newBlockingStub(channel).withDeadlineAfter(3, TimeUnit.SECONDS);

            new Thread(() -> {
                try {
                    switch (v.getId()) {
                        case R.id.video_btn_zoom_in: {
                            M11Control.LensGeneralInfo infoZoom = stub.startBackwardZoom(M11Control.ZoomQ.newBuilder().setZoomSpeed(seek_speed).build());
                            M11Control.LensGeneralInfo infoStop = stub.stopZoom(M11Control.Empty.newBuilder().build());
                            break;
                        }
                        case R.id.video_btn_zoom_out: {
                            M11Control.LensGeneralInfo infoZoom = stub.startForwardZoom(M11Control.ZoomQ.newBuilder().setZoomSpeed(seek_speed).build());
                            M11Control.LensGeneralInfo infoStop = stub.stopZoom(M11Control.Empty.newBuilder().build());
                            break;
                        }
                        case R.id.video_btn_focus_in: {
                            M11Control.LensGeneralInfo infoFocus = stub.startForwardFocus(M11Control.FocusQ.newBuilder().setFocusSpeed(seek_speed).build());
                            M11Control.LensGeneralInfo infoStop = stub.stopFocus(M11Control.Empty.newBuilder().build());
                            break;
                        }
                        case R.id.video_btn_focus_out: {
                            M11Control.LensGeneralInfo infoFocus = stub.startBackwardFocus(M11Control.FocusQ.newBuilder().setFocusSpeed(seek_speed).build());
                            M11Control.LensGeneralInfo infoStop = stub.stopFocus(M11Control.Empty.newBuilder().build());
                            break;
                        }
                        case R.id.video_btn_ir_filter_on: {
                            M11Control.LensGeneralInfo infoFilter = stub.openIRCutFilter(M11Control.Empty.newBuilder().build());
                            break;
                        }
                        case R.id.video_btn_ir_filter_off: {
                            M11Control.LensGeneralInfo infoFilter = stub.closeIRCutFilter(M11Control.Empty.newBuilder().build());
                            break;
                        }
                        case R.id.video_btn_calibration: {
                            M11Control.Empty empty;
                            if (calibmodes != null) {
                                int pos = calibmodes.getSelectedItemPosition();
                                empty = stub.startCalibration(
                                        M11Control.CalibrationQ.newBuilder().setMode(M11Control.CalibrationQ.CalibrationModes.forNumber(pos)).build());
                            } else
                                empty = stub.startCalibration(
                                        M11Control.CalibrationQ.newBuilder().setMode(M11Control.CalibrationQ.CalibrationModes.C_MANUAL).build());
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
    };

    private int seek_speed = 1;

    private final SeekBar.OnSeekBarChangeListener onSeekChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (progress == 0)
                return;

            seek_speed = progress;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private TableLayout btnLayout;

    private final View.OnClickListener onToggleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (btnLayout != null)
                btnLayout.setVisibility(btnLayout.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
        }
    };

    public AppCompatSpinner calibmodes;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player, container, false);

        if (mCam.getType() == CamModel.STREAM_TYPE.RTSP) {
            final ArrayList<String> args = new ArrayList<>();
            args.add("-vvv");
            args.add("--aout=opensles");
            args.add("--audio-time-stretch");
            args.add("--http-reconnect");
            args.add("--network-caching=" + 3 * 100);
            args.add("--rtsp-tcp");
            mLibVLC = new LibVLC(mContext, args);
            mMediaPlayer = new MediaPlayer(mLibVLC);
            mMediaPlayer.setEventListener(eventListener);
        }

        mVideoLayout = view.findViewById(R.id.video_layout);
        mVideoSurface = view.findViewById(R.id.video_surface);

        switch (mCam.getType()) {
            case RTSP:
                mVideoLayout.setVisibility(View.VISIBLE);
                mVideoSurface.setVisibility(View.GONE);
                break;
            case GRPC:
                mVideoLayout.setVisibility(View.GONE);
                mVideoSurface.setVisibility(View.VISIBLE);
                break;
        }

        zoom_focus_pos = view.findViewById(R.id.txt_zoom_focus_pos);
        video_btn_toggle = view.findViewById(R.id.video_btn_toggle);
        video_btn_toggle.setEnabled(false);

        btnLayout = view.findViewById(R.id.video_btn_table);

        ((ImageButton) view.findViewById(R.id.video_btn_focus_in)).setOnClickListener(onPlayerButtonClickListener);
        ((ImageButton) view.findViewById(R.id.video_btn_focus_out)).setOnClickListener(onPlayerButtonClickListener);
        ((ImageButton) view.findViewById(R.id.video_btn_zoom_in)).setOnClickListener(onPlayerButtonClickListener);
        ((ImageButton) view.findViewById(R.id.video_btn_zoom_out)).setOnClickListener(onPlayerButtonClickListener);
        ((ImageButton) view.findViewById(R.id.video_btn_calibration)).setOnClickListener(onPlayerButtonClickListener);
        ((AppCompatButton) view.findViewById(R.id.video_btn_ir_filter_on)).setOnClickListener(onPlayerButtonClickListener);
        ((AppCompatButton) view.findViewById(R.id.video_btn_ir_filter_off)).setOnClickListener(onPlayerButtonClickListener);
        ((ImageButton) view.findViewById(R.id.video_btn_toggle)).setOnClickListener(onToggleClickListener);
        ((AppCompatSeekBar) view.findViewById(R.id.video_speed_seekBar)).setOnSeekBarChangeListener(onSeekChangeListener);

        calibmodes = (AppCompatSpinner) view.findViewById(R.id.video_spin_calibration_modes);
        String[] names = Utils.getNames(M11Control.CalibrationQ.CalibrationModes.class);
        calibmodes.setAdapter(new ArrayAdapter<String>(view.getContext(), R.layout.support_simple_spinner_dropdown_item, names));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if (context instanceof PlayerFragment.OnRestartRequestListener) {
            mListener = (PlayerFragment.OnRestartRequestListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCamListFragmentListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mMediaPlayer != null)
            mMediaPlayer.release();
        if (mLibVLC != null)
            mLibVLC.release();

        if (channel != null) {
            channel.shutdownNow();
            channel = null;
        }
    }

    private Handler mHandler = new Handler();

    @Override
    public void onStart() {
        super.onStart();

        if (mCam.getType() == CamModel.STREAM_TYPE.RTSP) {
            mMediaPlayer.attachViews(mVideoLayout, null, ENABLE_SUBTITLES, USE_TEXTURE_VIEW);

            final Media media = new Media(mLibVLC, Uri.parse(mCam.getEndpoint()));
            mMediaPlayer.setEventListener(eventListener);
            mMediaPlayer.setMedia(media);
            media.release();

            mMediaPlayer.play();
        } else if (mCam.getType() == CamModel.STREAM_TYPE.GRPC) {
            player = new GrpcVideoPlayer(mVideoSurface);
            player.setErrorListener(err -> {
                Timber.e(err);
            });
            player.setStateListener(state -> {
                Timber.i(state.name());
            });
            try {
                player.start(mCam.getHost(), mCam.getPort(), "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mHandler.post(runnableUpdateZoomFocus);

        myAppLogReader.Start();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.detachViews();
        }

        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }

        myAppLogReader.Stop();

        mHandler.removeCallbacks(runnableUpdateZoomFocus);
    }

    private Runnable runnableUpdateZoomFocus = new Runnable() {
        @Override
        public void run() {
            mHandler.removeCallbacks(runnableUpdateZoomFocus);

            if (channel != null) {
                VideoControlGrpc.VideoControlBlockingStub stub = VideoControlGrpc.newBlockingStub(channel).withDeadlineAfter(3, TimeUnit.SECONDS);
                try {
                    M11Control.PosQ fPos = stub.getFocusPos(M11Control.Empty.newBuilder().build());
                    M11Control.PosQ zPos = stub.getZoomPos(M11Control.Empty.newBuilder().build());
                    M11Control.SensorInfo sInfo = stub.getSensorInfo(M11Control.Empty.newBuilder().build());

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            String t = String.format("Focus: %s\nZoom: %s\nSharpness: %s", fPos.getPos(), zPos.getPos(), sInfo.getSharpnessValue());
                            zoom_focus_pos.setText(t);

                            if (zoom_focus_pos.getVisibility() != View.VISIBLE)
                                zoom_focus_pos.setVisibility(View.VISIBLE);
                            if (!video_btn_toggle.isEnabled())
                                video_btn_toggle.setEnabled(true);
                        }
                    });
                } catch (Exception ignored) {
                    new Handler(Looper.getMainLooper()).post(() -> {
                        zoom_focus_pos.setVisibility(View.GONE);
                        if (video_btn_toggle.isEnabled())
                            video_btn_toggle.setEnabled(false);
                    });
                }
                mHandler.postDelayed(runnableUpdateZoomFocus, 500);
            } else
                mHandler.postDelayed(runnableUpdateZoomFocus, 1500);
        }
    };

    private void showVideoStoppedDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setIcon(R.drawable.ic_warning_black_24dp);
        builder.setTitle(mContext.getString(R.string.stopped_video));
        builder.setMessage(mContext.getString(R.string.try_reconnect));
        builder.setCancelable(true);
        builder.setNegativeButton(android.R.string.no, null);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            if (mListener != null)
                mListener.onRestartRequest();
        });

        builder.create().show();
    }

    private VlcError vlc_error = new VlcError(new VlcError.OnChangeListener() {
        @Override
        public void onChange(VlcError.ERR_TYPE err) {
            String msg = "";
            switch (err) {
                case UNKNOWN:
                    msg = String.format("%s %s", mContext.getString(R.string.toast_vlc_err), mContext.getString(R.string.unknown_failed));
                    break;
                case CONNECTION:
                    msg = String.format("%s %s", mContext.getString(R.string.toast_vlc_err), mContext.getString(R.string.connection_failed));
                    break;
                case SESSION:
                    msg = String.format("%s %s", mContext.getString(R.string.toast_vlc_err), mContext.getString(R.string.rtsp_session_failed));
                    break;
                case DISCONNECT:
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            showVideoStoppedDialog();
                        }
                    });
                    break;
            }

            String finalMsg = msg;
            new Handler(Looper.getMainLooper()).post(() -> {
                Toast.makeText(mContext, finalMsg, Toast.LENGTH_SHORT).show();
            });

        }
    });

    private final MyLibVLCLogReader myAppLogReader = new MyLibVLCLogReader(new MyLibVLCLogReader.OnLogListener() {
        private String connection_failed = "Connection failed";
        private String rtsp_session_failed = "Failed to setup RTSP session";
        private String decoder_is_draining = "Decoder is draining";

        @Override
        public void onLogCapture(String line) {
            if (line.toLowerCase().contains(connection_failed.toLowerCase()))
                vlc_error.set_err(VlcError.ERR_TYPE.CONNECTION);
            else if (line.toLowerCase().contains(rtsp_session_failed.toLowerCase()))
                vlc_error.set_err(VlcError.ERR_TYPE.SESSION);
            else if (line.toLowerCase().contains(decoder_is_draining.toLowerCase()))
                vlc_error.set_err(VlcError.ERR_TYPE.DISCONNECT);
        }
    });


    private final MediaPlayer.EventListener eventListener = new MediaPlayer.EventListener() {
        @Override
        public void onEvent(MediaPlayer.Event event) {
            switch (event.type) {
                case MediaPlayer.Event.Opening:
                    vlc_error.set_err(VlcError.ERR_TYPE.UNKNOWN);
                    Toast.makeText(mContext, mContext.getString(R.string.opening), Toast.LENGTH_SHORT).show();
                    break;
                case MediaPlayer.Event.Stopped:
                    break;
            }
        }
    };

    private static class VlcError {
        public enum ERR_TYPE {
            UNKNOWN,
            CONNECTION,
            SESSION,
            DISCONNECT
        }

        static ERR_TYPE _err;

        static OnChangeListener _listener;

        public VlcError(OnChangeListener listener) {
            _listener = listener;
        }

        synchronized public void set_err(ERR_TYPE err) {
            _err = err;
            if (_listener != null && err != ERR_TYPE.UNKNOWN)
                _listener.onChange(_err);
        }

        synchronized public ERR_TYPE get_err() {
            return _err;
        }

        interface OnChangeListener {
            void onChange(ERR_TYPE err);
        }
    }

    public static final class MyLibVLCLogReader {
        private static final String TAG = MyLibVLCLogReader.class.getCanonicalName();

        private static String[] runClearCMD = new String[]{"logcat", "-c"};
        private static String[] runCMD = new String[]{"logcat", "*:D", "-e", "libvlc"};

        private Thread thread;
        private boolean isRun = true;

        private OnLogListener logListener;

        public MyLibVLCLogReader() {
        }

        public MyLibVLCLogReader(OnLogListener listener) {
            logListener = listener;
        }

        public void Start() {
            thread = new Thread(runnable);
            thread.start();
        }

        public void Stop() {
            isRun = false;
        }

        private Runnable runnable = new Runnable() {
            @Override
            public void run() {
                isRun = true;
                Log.v(TAG, "Begin listening logger");
                try {
                    // Clear logs
                    Runtime.getRuntime().exec(runClearCMD);

                    // Capture logs
                    Process process = Runtime.getRuntime().exec(runCMD);
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(process.getInputStream()));

                    String line;
                    while (isRun && (line = bufferedReader.readLine()) != null) {
                        if (logListener != null) logListener.onLogCapture(line);
                    }

                    bufferedReader.close();
                    process.destroy();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.v(TAG, "Exit listening logger");
            }
        };

        public interface OnLogListener {
            public void onLogCapture(String line);
        }
    }

    public interface OnRestartRequestListener {
        public void onRestartRequest();
    }
}
