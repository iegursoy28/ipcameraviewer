package com.sparsetechnology.ipcameraviewer;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class CamListFragment extends Fragment {
    private OnCamListFragmentListener mListener;

    private Context mContext;

    private CamModelRecyclerViewAdapter camListAdapter;

    public CamListFragment() {
    }

    @SuppressWarnings("unused")
    public static CamListFragment newInstance(int columnCount) {
        CamListFragment fragment = new CamListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cammodel_list, container, false);

        camListAdapter = new CamModelRecyclerViewAdapter(mContext, mListener);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.cam_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(camListAdapter);

        ImageButton editBtn = (ImageButton) view.findViewById(R.id.cam_edit_btn);
        editBtn.setOnClickListener((v -> {
            if (camListAdapter != null)
                camListAdapter.toggleEditMode();
        }));

        ImageButton addBtn = (ImageButton) view.findViewById(R.id.cam_add_btn);
        addBtn.setOnClickListener((v -> {
            if (this.getFragmentManager() != null) {
                CamAddFragment f = CamAddFragment.newInstance();
                f.setListener(cam -> {
                    if (camListAdapter != null)
                        camListAdapter.addCamModel(cam);
                });
                f.showNow(this.getFragmentManager(), "AddCam");
            }
        }));

        ((TextView) view.findViewById(R.id.app_version)).setText(String.format("v%s", BuildConfig.VERSION_NAME));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if (context instanceof OnCamListFragmentListener) {
            mListener = (OnCamListFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCamListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnCamListFragmentListener {
        void onClickInteraction(CamModel item);
    }
}
