package com.sparsetechnology.ipcameraviewer;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.util.Log;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import vms.Nvr;
import vms.NvrServiceGrpc;

public class GrpcVideoPlayer {
    private static final String TAG = GrpcVideoPlayer.class.getSimpleName();

    private static final Nvr.PlaybackChanges pcNone =
            Nvr.PlaybackChanges.newBuilder().setType(Nvr.PlaybackChanges.PlaybackChangesTypes.PC_NONE).build();
    private static final Nvr.PlaybackChanges pcSeekAbsolute =
            Nvr.PlaybackChanges.newBuilder().setType(Nvr.PlaybackChanges.PlaybackChangesTypes.PC_SEEK_ABSOLUTE).build();

    public enum VideoState {PLAYING, PAUSED, STOPPED, FINISHED, IDLE}

    private ManagedChannel channel;

    private PlayingTimeChangeListener timeChangeListener = null;
    private OverlayChangeListener overlayChangeListener = null;
    private StateChangeListener stateListener = null;
    private ErrorListener errorListener = null;

    private VideoState videoState = VideoState.IDLE;
    private H264Decoder h264Decoder;

    private String mHost;
    private int mPort;
    private String mUid;

    private boolean isReady = false;
    private StreamObserver<vms.Nvr.CameraStreamQ> queryStreamObserver = null;

    GrpcVideoPlayer(@NonNull SurfaceView surfaceView) {
        h264Decoder = new H264Decoder(surfaceView);
    }

    public void start(String host, int port, String uid) throws Exception {
        configure(host, port, uid);
        setVideoState(VideoState.PLAYING);
        play(true);
    }

    private boolean checkConfigure() {
        try {
            configure(mHost, mPort, mUid);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void configure(String host, int port, String uid) throws Exception {
        if (isReady)
            return;

        if (host == null || host.isEmpty() || host.length() < 7)
            throw new Exception("Host not correct type.");
        if (port <= 0)
            throw new Exception("Port not correct:");
        if (uid == null)
            uid = "";

        mHost = host;
        mPort = port;
        mUid = uid;

        channel = ManagedChannelBuilder.forAddress(mHost, mPort).usePlaintext().build();
        NvrServiceGrpc.NvrServiceStub stub = NvrServiceGrpc.newStub(channel);
        queryStreamObserver = stub.getCameraStream(cameraFrameStreamObserver);
        isReady = true;
    }

    public void play(boolean isLive) {
        if (queryStreamObserver != null && checkConfigure()) {
            setVideoState(VideoState.PLAYING);
            if (isLive) {
                queryStreamObserver.onNext(Nvr.CameraStreamQ.newBuilder()
                        .setUniqueId(mUid)
                        .addStreams(Nvr.CameraStream.newBuilder().setCodec(Nvr.CameraStream.CodecType.CODEC_H264).build())
                        .setBeginTs(0).build());
            } else
                queryStreamObserver.onNext(Nvr.CameraStreamQ.newBuilder()
                        .setUniqueId(mUid)
                        .addStreams(Nvr.CameraStream.newBuilder().setCodec(Nvr.CameraStream.CodecType.CODEC_H264).build())
                        .setPc(pcNone).build());
        }
    }

    public void pause() {
        setVideoState(VideoState.PAUSED);
    }

    public void setSeek(long begin_ts) {
        //Log.d(TAG, String.format("Seek to date: %s", HelperUtils.getDateFromEpoc(begin_ts)));
        Nvr.PlaybackChanges seekAbsolute = Nvr.PlaybackChanges.newBuilder(pcSeekAbsolute).setValue(begin_ts).build();

        if (queryStreamObserver != null && checkConfigure()) {
            Nvr.CameraStreamQ request = Nvr.CameraStreamQ.newBuilder()
                    .setUniqueId(mUid)
                    .setBeginTs(begin_ts)
                    .setPc(seekAbsolute)
                    .build();

            setVideoState(VideoState.PLAYING);
            queryStreamObserver.onNext(request);
        }
    }

    public void setPlaybackChange(Nvr.PlaybackChanges.PlaybackChangesTypes type, int value) {
        Nvr.PlaybackChanges playbackChanges;
        if (type == Nvr.PlaybackChanges.PlaybackChangesTypes.PC_PLAY_FAST && Math.abs(value) > 3)
            return;

        playbackChanges = Nvr.PlaybackChanges.newBuilder().setType(type).setValue(value).build();

        if (queryStreamObserver != null && checkConfigure()) {
            Nvr.CameraStreamQ request = Nvr.CameraStreamQ.newBuilder()
                    .setUniqueId(mUid)
                    .setPc(playbackChanges)
                    .build();

            setVideoState(VideoState.PLAYING);
            queryStreamObserver.onNext(request);
        }
    }

    public void stop() {
        if (channel != null) {
            channel.shutdownNow();
            channel = null;
        }
        isReady = false;
        setVideoState(VideoState.IDLE);
    }

    public void release() {
        if (h264Decoder != null)
            h264Decoder.releasePlayer();
    }

    private StreamObserver<Nvr.StreamFrame> cameraFrameStreamObserver = new StreamObserver<Nvr.StreamFrame>() {
        @Override
        public void onNext(Nvr.StreamFrame element) {
            try {
                if (overlayChangeListener != null && element.getDataCount() > 1)
                    overlayChangeListener.onChange(element.getData(1).getData().toStringUtf8());

                Nvr.StreamBuffer frame = element.getData(0);

                // Update time
                if (timeChangeListener != null)
                    timeChangeListener.onChange(frame.getTs());

                // Request next frame
                if (videoState == VideoState.PLAYING) {
                    // Use frame data
                    h264Decoder.useFrameData(frame.getData().toByteArray(), frame.getTs());

                    queryStreamObserver.onNext(Nvr.CameraStreamQ.newBuilder().setUniqueId(mUid).setPc(pcNone).build());
                    setVideoState(VideoState.PLAYING);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onError(Throwable t) {
            Status s = Status.fromThrowable(t);
            t.printStackTrace();
            if (errorListener != null)
                errorListener.onError(String.format("Error [%s]: %s", s.getCode(), s.getDescription()));
            stop();
        }

        @Override
        public void onCompleted() {
            setVideoState(VideoState.FINISHED);
        }
    };

    private void setVideoState(VideoState s) {
        if (videoState == s) return;
        videoState = s;
        if (stateListener != null)
            stateListener.onChange(videoState);
    }

    public void setStateListener(StateChangeListener stateListener) {
        this.stateListener = stateListener;
    }

    public void setOverlayChangeListener(OverlayChangeListener overlayChangeListener) {
        this.overlayChangeListener = overlayChangeListener;
    }

    public void setErrorListener(ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    public void setTimeChangeListener(PlayingTimeChangeListener timeChangeListener) {
        this.timeChangeListener = timeChangeListener;
    }

    public interface StateChangeListener {
        void onChange(VideoState state);
    }

    public interface OverlayChangeListener {
        void onChange(String data);
    }

    public interface ErrorListener {
        void onError(String err);
    }

    public interface PlayingTimeChangeListener {
        void onChange(long ts);
    }

    private static class H264Decoder {
        private static final String MIME_TYPE = MediaFormat.MIMETYPE_VIDEO_AVC;
        private static final int MEDIA_CODEC_WIDTH = 1920;
        private static final int MEDIA_CODEC_HEIGHT = 1080;
        private static final int TIMEOUT_U_SEC = 100;

        private MediaCodec mMediaCodec = null;
        private byte[] mSPS = null;
        private byte[] mPPS = null;

        private final SurfaceView surfaceView;
        private boolean isRun;

        public H264Decoder(SurfaceView surfaceView) {
            this.surfaceView = surfaceView;
            isRun = true;
        }

        private MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();

        public void useFrameData(byte[] data, long ts) {
            if (mMediaCodec == null) {
                mMediaCodec = prepareDecoder(data);
                return;
            }

            int inIndex;
            while ((inIndex = mMediaCodec.dequeueInputBuffer(TIMEOUT_U_SEC)) < 0)
                if (!isRun) return;

            if (inIndex > 0) {
                Objects.requireNonNull(mMediaCodec.getInputBuffer(inIndex)).put(data);
                mMediaCodec.queueInputBuffer(inIndex, 0, data.length, 0, MediaCodec.CRYPTO_MODE_UNENCRYPTED);

                int outIndex = mMediaCodec.dequeueOutputBuffer(bufferInfo, TIMEOUT_U_SEC);
                switch (outIndex) {
                    case MediaCodec.INFO_TRY_AGAIN_LATER:
                        Log.d("Buffer info", "INFO_TRY_AGAIN_LATER");
                        break;
                    case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                        Log.d("Buffer info", "INFO_OUTPUT_FORMAT_CHANGED: New format: " + mMediaCodec.getOutputFormat());
                        break;
                    case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                        Log.d("Buffer info", "INFO_OUTPUT_BUFFERS_CHANGED");
                        break;
                    default:
                        mMediaCodec.releaseOutputBuffer(outIndex, true);
                        break;
                }
            }
        }

        public void releasePlayer() {
            isRun = false;

            if (mMediaCodec != null) {
                mMediaCodec.setParameters(null);
                mMediaCodec.stop();
                mMediaCodec.release();
            }
        }

        private MediaCodec prepareDecoder(byte[] data) {
            if (mPPS == null || mSPS == null) {
                mSPS = HelperUtils.findSPS(data);
                mPPS = HelperUtils.findPPS(data);

                return null;
            }

            MediaCodec mDecoder;
            MediaFormat mediaFormat = MediaFormat.createVideoFormat(MIME_TYPE, MEDIA_CODEC_WIDTH, MEDIA_CODEC_HEIGHT);
            mediaFormat.setByteBuffer("csd-0", ByteBuffer.wrap(mSPS));
            mediaFormat.setByteBuffer("csd-1", ByteBuffer.wrap(mPPS));

            try {
                mDecoder = MediaCodec.createDecoderByType(MIME_TYPE);
                mDecoder.configure(mediaFormat, surfaceView.getHolder().getSurface(), null, 0);
                mDecoder.start();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            // Prepared decoder
            isRun = true;

            return mDecoder;
        }

        public interface OnFrameRateChangeListener {
            void onChange(float fps);
        }
    }
}