package com.sparsetechnology.ipcameraviewer;

import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;

public class CamModel {
    public enum STREAM_TYPE {GRPC, RTSP}

    private STREAM_TYPE type;

    private String host;
    private int port;

    private String url;

    public static CamModel newInstanceRTPS(String url) {
        String h = null;
        try {
            URI uri = new URI(url);
            h = uri.getHost();
        } catch (URISyntaxException ignored) {
        }
        return new CamModel(STREAM_TYPE.RTSP, h != null ? h : "", 40001, url);
    }

    public static CamModel newInstanceGRPC(String host, int port) {
        return new CamModel(STREAM_TYPE.GRPC, host, port, "");
    }

    private CamModel(STREAM_TYPE type, String host, int port, String url) {
        this.type = type;
        this.host = host;
        this.port = port;
        this.url = url;
    }

    public STREAM_TYPE getType() {
        return type;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getEndpoint() {
        if (type == STREAM_TYPE.RTSP)
            return url;
        else if (type == STREAM_TYPE.GRPC)
            return String.format("%s:%s", host, port);
        else
            return "";
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static CamModel fromJson(String json) {
        return new Gson().fromJson(json, CamModel.class);
    }
}
